defmodule App.Release.Tasks do
  @doc """
  This is a migration hook to work around Mix not being available in production.
  """

  def migrate do
    {:ok, _} = Application.ensure_all_started(:app)

    app_path = Application.app_dir(:app, "priv/repo/migrations")

    Ecto.Migrator.run(App.Repo, app_path, :up, all: true)
  end
end
