defmodule AppWeb.CourtAppearanceController do
  use AppWeb, :controller

  alias App.Cases
  alias App.Cases.CourtAppearance

  def index(conn, _params) do
    court_appearances = Cases.list_court_appearances()
    render(conn, "index.html", court_appearances: court_appearances)
  end

  def new(conn, _params) do
    changeset = Cases.change_court_appearance(%CourtAppearance{})
    render(conn, "new.html", changeset: changeset, defendants: get_defendants())
  end

  def create(conn, %{"court_appearance" => court_appearance_params}) do
    case Cases.create_court_appearance(court_appearance_params) do
      {:ok, court_appearance} ->
        conn
        |> put_flash(:info, "Court appearance created successfully.")
        |> redirect(to: court_appearance_path(conn, :show, court_appearance))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset, defendants: get_defendants())
    end
  end

  defp get_defendants, do: Cases.list_defendants() |> Enum.map(&{&1.name, &1.id})

  def show(conn, %{"id" => id}) do
    court_appearance = Cases.get_court_appearance!(id)
    render(conn, "show.html", court_appearance: court_appearance)
  end

  def edit(conn, %{"id" => id}) do
    court_appearance = Cases.get_court_appearance!(id)
    changeset = Cases.change_court_appearance(court_appearance)
    render(conn, "edit.html", court_appearance: court_appearance, changeset: changeset, defendants: get_defendants())
  end

  def update(conn, %{"id" => id, "court_appearance" => court_appearance_params}) do
    court_appearance = Cases.get_court_appearance!(id)

    case Cases.update_court_appearance(court_appearance, court_appearance_params) do
      {:ok, court_appearance} ->
        conn
        |> put_flash(:info, "Court appearance updated successfully.")
        |> redirect(to: court_appearance_path(conn, :show, court_appearance))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", court_appearance: court_appearance, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    court_appearance = Cases.get_court_appearance!(id)
    {:ok, _court_appearance} = Cases.delete_court_appearance(court_appearance)

    conn
    |> put_flash(:info, "Court appearance deleted successfully.")
    |> redirect(to: court_appearance_path(conn, :index))
  end
end
