defmodule AppWeb.DefendantController do
  use AppWeb, :controller

  alias App.Cases
  alias App.Cases.Defendant

  def index(conn, _params) do
    defendants = Cases.list_defendants()
    render(conn, "index.html", defendants: defendants)
  end

  def new(conn, _params) do
    changeset = Cases.change_defendant(%Defendant{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"defendant" => defendant_params}) do
    case Cases.create_defendant(defendant_params) do
      {:ok, defendant} ->
        conn
        |> put_flash(:info, "Defendant created successfully.")
        |> redirect(to: defendant_path(conn, :show, defendant))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    defendant = Cases.get_defendant!(id)
    render(conn, "show.html", defendant: defendant)
  end

  def edit(conn, %{"id" => id}) do
    defendant = Cases.get_defendant!(id)
    changeset = Cases.change_defendant(defendant)
    render(conn, "edit.html", defendant: defendant, changeset: changeset)
  end

  def update(conn, %{"id" => id, "defendant" => defendant_params}) do
    defendant = Cases.get_defendant!(id)

    case Cases.update_defendant(defendant, defendant_params) do
      {:ok, defendant} ->
        conn
        |> put_flash(:info, "Defendant updated successfully.")
        |> redirect(to: defendant_path(conn, :show, defendant))
      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", defendant: defendant, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    defendant = Cases.get_defendant!(id)
    {:ok, _defendant} = Cases.delete_defendant(defendant)

    conn
    |> put_flash(:info, "Defendant deleted successfully.")
    |> redirect(to: defendant_path(conn, :index))
  end
end
