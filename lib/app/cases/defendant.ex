defmodule App.Cases.Defendant do
  use Ecto.Schema
  import Ecto.Changeset
  alias App.Cases.{CourtAppearance, Defendant}


  schema "defendants" do
    field :name, :string
    has_many :court_appearances, CourtAppearance

    timestamps()
  end

  @doc false
  def changeset(%Defendant{} = defendant, attrs) do
    defendant
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end
end
