defmodule App.Cases do
  @moduledoc """
  The Cases context.
  """

  import Ecto.Query, warn: false
  alias App.Repo

  alias App.Cases.Defendant

  @doc """
  Returns the list of defendants.

  ## Examples

      iex> list_defendants()
      [%Defendant{}, ...]

  """
  def list_defendants do
    Repo.all(Defendant)
  end

  @doc """
  Gets a single defendant.

  Raises `Ecto.NoResultsError` if the Defendant does not exist.

  ## Examples

      iex> get_defendant!(123)
      %Defendant{}

      iex> get_defendant!(456)
      ** (Ecto.NoResultsError)

  """
  def get_defendant!(id), do: Repo.get!(Defendant, id)

  @doc """
  Creates a defendant.

  ## Examples

      iex> create_defendant(%{field: value})
      {:ok, %Defendant{}}

      iex> create_defendant(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_defendant(attrs \\ %{}) do
    %Defendant{}
    |> Defendant.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a defendant.

  ## Examples

      iex> update_defendant(defendant, %{field: new_value})
      {:ok, %Defendant{}}

      iex> update_defendant(defendant, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_defendant(%Defendant{} = defendant, attrs) do
    defendant
    |> Defendant.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Defendant.

  ## Examples

      iex> delete_defendant(defendant)
      {:ok, %Defendant{}}

      iex> delete_defendant(defendant)
      {:error, %Ecto.Changeset{}}

  """
  def delete_defendant(%Defendant{} = defendant) do
    Repo.delete(defendant)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking defendant changes.

  ## Examples

      iex> change_defendant(defendant)
      %Ecto.Changeset{source: %Defendant{}}

  """
  def change_defendant(%Defendant{} = defendant) do
    Defendant.changeset(defendant, %{})
  end

  alias App.Cases.CourtAppearance

  @doc """
  Returns the list of court_appearances.

  ## Examples

      iex> list_court_appearances()
      [%CourtAppearance{}, ...]

  """
  def list_court_appearances do
    Repo.all(CourtAppearance) 
    |> Repo.preload(:defendant)
    |> Enum.sort(&sort_dates/2)
  end

  defp sort_dates(c1, c2)  do
    case Date.compare(to_date(c1.date), to_date(c2.date)) do
      :lt -> true
      _ -> false # >=
    end
  end

  @months %{
    "January" => 1,
    "February" => 2,
    "March" => 3,
    "April" => 4,
    "May" => 5,
    "June" => 6,
    "July" => 7,
    "August" => 8,
    "September" => 9,
    "October" => 10,
    "November" => 11,
    "December" => 12
  }

  defp to_date(date_string) do
    parts = 
      date_string
      |> String.split(" ", trim: true)
      |> Enum.with_index
      |> Enum.map(fn 
        {part, 1} -> String.replace(part, ",", "")
        {part, _} -> part
      end)
    {:ok, date} = Date.new(Enum.at(parts, 2) |> String.to_integer, @months[Enum.at(parts, 1)], Enum.at(parts, 0) |> String.to_integer)
    date
  end

  @doc """
  Gets a single court_appearance.

  Raises `Ecto.NoResultsError` if the Court appearance does not exist.

  ## Examples

      iex> get_court_appearance!(123)
      %CourtAppearance{}

      iex> get_court_appearance!(456)
      ** (Ecto.NoResultsError)

  """
  def get_court_appearance!(id), do: Repo.get!(CourtAppearance, id)

  @doc """
  Creates a court_appearance.

  ## Examples

      iex> create_court_appearance(%{field: value})
      {:ok, %CourtAppearance{}}

      iex> create_court_appearance(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_court_appearance(attrs \\ %{}) do
    %CourtAppearance{}
    |> CourtAppearance.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a court_appearance.

  ## Examples

      iex> update_court_appearance(court_appearance, %{field: new_value})
      {:ok, %CourtAppearance{}}

      iex> update_court_appearance(court_appearance, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_court_appearance(%CourtAppearance{} = court_appearance, attrs) do
    court_appearance
    |> CourtAppearance.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a CourtAppearance.

  ## Examples

      iex> delete_court_appearance(court_appearance)
      {:ok, %CourtAppearance{}}

      iex> delete_court_appearance(court_appearance)
      {:error, %Ecto.Changeset{}}

  """
  def delete_court_appearance(%CourtAppearance{} = court_appearance) do
    Repo.delete(court_appearance)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking court_appearance changes.

  ## Examples

      iex> change_court_appearance(court_appearance)
      %Ecto.Changeset{source: %CourtAppearance{}}

  """
  def change_court_appearance(%CourtAppearance{} = court_appearance) do
    CourtAppearance.changeset(court_appearance, %{})
  end
end
