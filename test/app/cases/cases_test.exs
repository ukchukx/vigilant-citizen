defmodule App.CasesTest do
  use App.DataCase

  alias App.Cases

  describe "defendants" do
    alias App.Cases.Defendant

    @valid_attrs %{name: "some name"}
    @update_attrs %{name: "some updated name"}
    @invalid_attrs %{name: nil}

    def defendant_fixture(attrs \\ %{}) do
      {:ok, defendant} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Cases.create_defendant()

      defendant
    end

    test "list_defendants/0 returns all defendants" do
      defendant = defendant_fixture()
      assert Cases.list_defendants() == [defendant]
    end

    test "get_defendant!/1 returns the defendant with given id" do
      defendant = defendant_fixture()
      assert Cases.get_defendant!(defendant.id) == defendant
    end

    test "create_defendant/1 with valid data creates a defendant" do
      assert {:ok, %Defendant{} = defendant} = Cases.create_defendant(@valid_attrs)
      assert defendant.name == "some name"
    end

    test "create_defendant/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cases.create_defendant(@invalid_attrs)
    end

    test "update_defendant/2 with valid data updates the defendant" do
      defendant = defendant_fixture()
      assert {:ok, defendant} = Cases.update_defendant(defendant, @update_attrs)
      assert %Defendant{} = defendant
      assert defendant.name == "some updated name"
    end

    test "update_defendant/2 with invalid data returns error changeset" do
      defendant = defendant_fixture()
      assert {:error, %Ecto.Changeset{}} = Cases.update_defendant(defendant, @invalid_attrs)
      assert defendant == Cases.get_defendant!(defendant.id)
    end

    test "delete_defendant/1 deletes the defendant" do
      defendant = defendant_fixture()
      assert {:ok, %Defendant{}} = Cases.delete_defendant(defendant)
      assert_raise Ecto.NoResultsError, fn -> Cases.get_defendant!(defendant.id) end
    end

    test "change_defendant/1 returns a defendant changeset" do
      defendant = defendant_fixture()
      assert %Ecto.Changeset{} = Cases.change_defendant(defendant)
    end
  end

  describe "court_appearances" do
    alias App.Cases.CourtAppearance

    @valid_attrs %{date: "some date", remarks: "some remarks"}
    @update_attrs %{date: "some updated date", remarks: "some updated remarks"}
    @invalid_attrs %{date: nil, remarks: nil}

    def court_appearance_fixture(attrs \\ %{}) do
      {:ok, court_appearance} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Cases.create_court_appearance()

      court_appearance
    end

    test "list_court_appearances/0 returns all court_appearances" do
      court_appearance = court_appearance_fixture()
      assert Cases.list_court_appearances() == [court_appearance]
    end

    test "get_court_appearance!/1 returns the court_appearance with given id" do
      court_appearance = court_appearance_fixture()
      assert Cases.get_court_appearance!(court_appearance.id) == court_appearance
    end

    test "create_court_appearance/1 with valid data creates a court_appearance" do
      assert {:ok, %CourtAppearance{} = court_appearance} = Cases.create_court_appearance(@valid_attrs)
      assert court_appearance.date == "some date"
      assert court_appearance.remarks == "some remarks"
    end

    test "create_court_appearance/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Cases.create_court_appearance(@invalid_attrs)
    end

    test "update_court_appearance/2 with valid data updates the court_appearance" do
      court_appearance = court_appearance_fixture()
      assert {:ok, court_appearance} = Cases.update_court_appearance(court_appearance, @update_attrs)
      assert %CourtAppearance{} = court_appearance
      assert court_appearance.date == "some updated date"
      assert court_appearance.remarks == "some updated remarks"
    end

    test "update_court_appearance/2 with invalid data returns error changeset" do
      court_appearance = court_appearance_fixture()
      assert {:error, %Ecto.Changeset{}} = Cases.update_court_appearance(court_appearance, @invalid_attrs)
      assert court_appearance == Cases.get_court_appearance!(court_appearance.id)
    end

    test "delete_court_appearance/1 deletes the court_appearance" do
      court_appearance = court_appearance_fixture()
      assert {:ok, %CourtAppearance{}} = Cases.delete_court_appearance(court_appearance)
      assert_raise Ecto.NoResultsError, fn -> Cases.get_court_appearance!(court_appearance.id) end
    end

    test "change_court_appearance/1 returns a court_appearance changeset" do
      court_appearance = court_appearance_fixture()
      assert %Ecto.Changeset{} = Cases.change_court_appearance(court_appearance)
    end
  end
end
