import MainView from '../main';

export default class View extends MainView {
  mount() {
    super.mount();
    console.log('IndexView mounted');
    this.timelineBank = {};
    const elems = document.querySelectorAll('#timeline-bank > div');
    elems.forEach((el) => {
    	let remark = "", date = "";
    	const remTag = el.querySelector('p.remark'), dateTag = el.querySelector('p.date');
    	if (remTag) remark = remTag.innerHTML;
    	if (dateTag) date = dateTag.innerHTML;
    	const list = this.timelineBank[el.dataset.defendantId] || [];
    	if (remark.length && date.length) list.push({ remark, date });
    	this.timelineBank[el.dataset.defendantId] = list;
    });
    // console.log(this.timelineBank);

    this.defendants = document.querySelectorAll('.menu-box .menu-box-item > a');
    // console.log(this.defendants);
    this.timelineElem = document.querySelector('#wallmessages');
    this.opinionElem = document.querySelector('#opinions');
    this.defendants.forEach(item => item.addEventListener('click', this.handleListItemClick.bind(this), false));
    this.eventFire(this.defendants[0], 'click'); // click first item
    setTimeout(readWelcomeMessage, 1000);
  }

  handleListItemClick(e) {
  	this.timelineElem.innerHTML = '';
  	this.defendants.forEach(item => item.parentElement.classList.remove('active'));
  	e.target.parentElement.classList.add('active');
  	const defendantId = e.target.dataset.defendantId;
  	const bank = this.timelineBank[e.target.dataset.defendantId] || [];
  	bank.forEach(item => this.timelineElem.appendChild(this.timelineItem(item)));
    
    // Fetch tweets
    this.opinionElem.innerHTML = '';
    let url, caseTitle = e.target.innerHTML.toLowerCase();

    if (caseTitle.indexOf('dasuki') !== -1) {
      url = 'http://samie820.pythonanywhere.com/api/tweets/dasuki';
    } else if (caseTitle.indexOf('olisa') !== -1 || caseTitle.indexOf('metuh') !== -1) {
      url = 'http://samie820.pythonanywhere.com/api/tweets/olisa';
    } else if (caseTitle.indexOf('allison') !== -1 || caseTitle.indexOf('madueke') !== -1) {
      url = 'http://samie820.pythonanywhere.com/api/tweets/allison';
    }

    if (url) {
      this.fetchJSON(url)
      .then(tweets => {
        tweets.slice(0,10).forEach(tweet => this.opinionElem.appendChild(this.createElementFromTweet(tweet)));
      });
    }
  }

  fetchJSON(url) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest(); // TODO: Add withCredentials
      xhr.open('GET', url);
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(JSON.parse(xhr.response));
        } else {
          reject(xhr.response);
        }
      };
      xhr.onerror = () => reject(xhr.response);
      xhr.send();
    })
    .then(resp => resp.map(item => item.tweets));
  
  }

  createElementFromTweet(text) {
    const 
      li = document.createElement('li'),
      personaBox = document.createElement('div'),
      personaSpan = document.createElement('span'),
      infoDiv = document.createElement('div'),
      avatar = document.createElement('a'),
      avatarImg = document.createElement('img'),
      p = document.createElement('p');

    personaBox.className = 'person-box';
    personaSpan.className = 'persona-avatar-box ultrapill cl-watcher-avatar-box';
    infoDiv.className = 'persona-info';
    avatar.className = 'persona-avatar';
    avatarImg.className = 'flexible';

    avatar.href = '#';
    avatarImg.src = '/images/twitterlogo.png';
    avatarImg.alt = 'Twitter logo';
    avatar.appendChild(avatarImg);
    personaSpan.appendChild(avatar);

    p.innerHTML = text;
    infoDiv.appendChild(p);
    personaBox.appendChild(personaSpan);
    personaBox.appendChild(infoDiv);
    li.appendChild(personaBox);
    return li;
  }

  timelineItem({ remark, date }) {
  	const 
  		messageItem = document.createElement('div'),
  		messageContent = document.createElement('div'),
  		messageHead = document.createElement('div'),
      messageInner = document.createElement('div'),
  		userDetail = document.createElement('div'),
  		handle = document.createElement('h5');


  	messageInner.className = 'message-inner';
    messageItem.className = 'message-item';
    messageHead.className = 'message-head clearfix';
    messageContent.className = 'qa-message-content';
    userDetail.className = 'user-detail';
    handle.className = 'handle';
    
    handle.innerHTML = date;
    userDetail.appendChild(handle);
    messageHead.appendChild(userDetail);

    messageContent.innerHTML = remark;
    messageInner.appendChild(messageHead);
    messageInner.appendChild(messageContent);

  	messageItem.appendChild(messageInner);

  	return messageItem;
  }

  eventFire(el, etype){
	  if (el.fireEvent) {
	    el.fireEvent('on' + etype);
	  } else {
	    var evObj = document.createEvent('Events');
	    evObj.initEvent(etype, true, false);
	    el.dispatchEvent(evObj);
	  }
	}

  unmount() {
    super.unmount();
    this.defendants.forEach(item => item.removeEventListener('click', this.handleListItemClick.bind(this), false));
    console.log('IndexView unmounted');
  }
}

