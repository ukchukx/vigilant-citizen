import MainView from '../main';

export default class View extends MainView {
  mount() {
    super.mount();
    this.readButtons = document.querySelectorAll('.read');
    this.readButtons.forEach(button => button.addEventListener('click', this.handleReadClick.bind(this), false));
    console.log('LearnView mounted');
  }

  handleReadClick(e) {
    const 
      isPlaying = e.target.innerHTML.indexOf('Stop') !== -1,
      textElem = document.querySelector(`#modal-${e.target.dataset.articleId}-text`),
      text = textElem ? textElem.innerHTML : '';

    if (!isPlaying) {
      sayIt(text);
      e.target.innerHTML = 'Stop reading';
    } else {
      e.target.innerHTML = 'Read to me';
      stopSayingIt();
    }
  }


  unmount() {
    super.unmount();
    this.readButtons.forEach(button => button.removeEventListener('click', this.handleReadClick.bind(this), false));
    console.log('LearnView unmounted');
  }

  closest(el, selector, stopSelector) {
    let retval = null;
    while (el) {
      if (el.matches(selector)) {
        retval = el;
        break;
      } else if (stopSelector && el.matches(stopSelector)) {
        break;
      }
      el = el.parentElement;
    }
    return retval;
  }
}

