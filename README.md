# VigilantCitizens

To start:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create && mix ecto.migrate`
  * Install Node.js dependencies with `cd assets && npm install`
  * Start Phoenix endpoint with `mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

## Description
VigilantCitizens is a web-based action-focused innovation for social change that
provides data on the tracking of and education on corruption cases and the
dangers/ills of corruption to society respectively.
